%%  S4 to S11
%
% Author: Daniel R. Gallagher
% Date  : 2008/12/2
%
%   This function strips extraneous S-parameters from a data file.
%
%   The function converts a two-port data file to a one-port data file most
%   useful when a 2-port calibration is used out of convenience when
%   measuring a 1-port device.  The remaining S-parameters typically
%   contain noise and interfere with the ability to compare data to a
%   simulation from within the COM model.

%% Import 2-port Data File
function s4tos11
    
%open file
if nargin==0
    [FileName,PathName] = uigetfile('*.txt','Select the data file');
    %if user clicks cancel, quit
    if PathName == 0
        return;
    end
    %fid = fopen( strcat(PathName,FileName) ,'rt');
    
    imp_data=importdata(strcat(PathName,FileName));
else
    imp_data=importdata(input);
end

%% Assign Data to a file
%data_out= [imp_data.data(:,1), imp_data.data(:,2), imp_data.data(:,3)];


%% Output New, 1-port Data File
        [OutputFileName,OutputPathName] = UIPUTFILE('*.txt', 'Save File As...', ...
             FileName(1:length(FileName)-4));
        if PathName == 0
            return;
        end
    FileOut=strcat(OutputPathName, OutputFileName);


fid=fopen(FileOut,'wt');
fprintf(fid,'Freq, Real(S11), Imag(S11)\n');
for i = 1:length(imp_data.data(:,1))
    fprintf(fid,'%12.8f, %12.8f, %12.8f\n', imp_data.data(i,1), imp_data.data(i,2), imp_data.data(i,3));
end

fclose(fid);

return;    
