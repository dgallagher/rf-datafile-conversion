%% Data Aquision File format Converter
%
% Author: Daniel R. Gallagher
% Date  : 2008/10/7
%
% This conversion program converts the data acquisition file format to touchstone.
% Conversion is useful for use of native RF Toolbox functions in Matlab.
%
% Although the touchstone file format supports unlimited-port networks,
% this function will only convert 1- or 2-port network S-parameters.
%
% Data files from the Data Acquisition Matlab program written by Rick Puccio
% output files S-parameters in Real-Imaginary format. For handling other 
% data types (such as Mag-Angle) this program will have to be modified to 
% detect this change and output a different touchstone header line.
%
% Touchstone file definition notes:
%  1. Touchstone file are case-insensitive.
%  2. Only ASCII characters, as defined in ANSI Standard X3.4-1986, may be
%     used in a Touchstone file. The use of characters with codes greater
%     than hexadecimal 07E is not allowed. Also, ASCII control characters
%     (those numerically less than hexadecimal 20) are not allowed, except
%     for tabs or in a line termination sequence (carriage-return or
%     carriage-return/line-feed combination).
%          Note: The use of Tab characters is strongly discouraged.
%  3. Comments are preceded by an exclamation mark (!). Comments may
%     appear on a separate line, or after the last data value on a line.
%     Comments are terminated by a line termination sequence (i.e. multi-
%     line comments are not allowed).
%  4. By convention, Touchstone filenames use a file extension of ‘.snp’,
%     where "n"? is the number of network ports of the device or
%     interconnect being described. For example, a Touchstone file
%     containing the network parameters for a two port device would be
%     named ‘filename’.s2p, while a Touchstone file containing the data
%     for a 3-port network would be ‘filename’.s3p, and so on.
%  5. By convention, angles are measured in degrees.

%% Import Data File
%function s2p_Output=touchtone(input, output)
function touchstone(input, output)

%open file
if nargin==0
    [FileName,PathName] = uigetfile('*.txt','Select the data file');
    %if user clicks cancel, quit
    if PathName == 0
        return;
    end
    %fid = fopen( strcat(PathName,FileName) ,'rt');
    
    imp_data=importdata(strcat(PathName,FileName));
else
    imp_data=importdata(input);
end


%% Process Raw Data

freq_vec = imp_data.data(:,1);
len_data = length(imp_data.data);
switch(length(imp_data.colheaders))
    case 9 % 2-port Data Aquisition File
        s_vec = zeros(2,2,len_data);
        for loop = 1:len_data
            s_vec(:,:,loop) = [imp_data.data(loop,2)+imp_data.data(loop,3)*i, ...
                               imp_data.data(loop,4)+imp_data.data(loop,5)*i; ...
                               imp_data.data(loop,6)+imp_data.data(loop,7)*i, ...
                               imp_data.data(loop,8)+imp_data.data(loop,9)*i];
        end      
    case 3 % 1-port Data Aquisition File
        %s_vec = zeros(1,1,len_data);
        for loop = 1:len_data
            s_vec(:,:,loop) = imp_data.data(loop,2)+imp_data.data(loop,3)*i;
        end
    otherwise
        errordlg('There is an error in your data-file.','Incorect Data Dimensions')
        return
end  
       
%% Output Data as Touchstone file format
% Note: This requires a high level objects provided by the RF Toolbox
% This code can be rewritten to remove this dependency

% Create an rfdata.data object called txdata with the default property values:
txdata = rfdata.data;

% Set the S-parameter values of txdata to the values you specified in s_vec:
txdata.S_Parameters = s_vec;

% Set the frequency values of txdata to freq_vec

txdata.Freq = freq_vec;

% Export the data in txdata to a Touchstone file called test.s2p:

%open file
if nargin < 2
    switch(length(imp_data.colheaders))
    case 9 % 2-port Data Aquisition File
        [OutputFileName,OutputPathName] = uiputfile('*.s2p', 'Save Touchstone File As...', ...
            FileName(1:length(FileName)-4));
        if PathName == 0
            return;
        end
    case 3 % 1-port Data Aquisition File
        [OutputFileName,OutputPathName] = UIPUTFILE('*.s1p', 'Save Touchstone File As...', ...
            FileName(1:length(FileName)-4));
        if PathName == 0
            return;
        end
    otherwise
        errordlg('There is an error in your data-file.','Incorect Data Dimensions')
        return
    end
    FileOut=strcat(OutputPathName, OutputFileName);
else
    % If filename arguments passed to function
    FileOut = output;
end

write(txdata, FileOut);
return;    
